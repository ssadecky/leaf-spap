# Boilerplate with standalone laravel-mix and bootstrap

This is just a very simple boilerplate to quick start any mini web project using 
laravel mix, laravel valet, bootstrap and sass, js preprocessors with browsersync.


## Installation



### To install all dependencies:

```
npm install
```

## Usage

Run all tasks:

```
npm run dev
```

Run all tasks and minify:

```
npm run production
```

Run all tasks and watch for changes (first run this will install additional dependencies):

```
npm run watch
```