/**
 * Main js file
 */
import 'babel-polyfill'
import 'nodelist-foreach-polyfill'
import 'url-search-params-polyfill'

import $ from 'jquery'
import Odometer from 'odometer'
import AOS from 'aos'

import formAnimation from './utils/formAnimation'
import toggleClass from './utils/toggleClass'
import toggle from './utils/toggle'

import { each } from './helpers/helpers'

import Swipers from './components/Swipers'
import Accordion from './components/Accordion'
import Menu from './components/Menu'
import Map from './components/Map'

import pedInit from './loaders/pedInit'

import { mapOptionsDetail, desktopMapOptionsCommunity, mobileMapOptionsCommunity } from './const/map'

$(document).ready(() => {
    init()
})

function init() {
    const primarySearchInput = document.querySelector('.js-primary-search')

    if (primarySearchInput) {
        document.querySelector('.js-primary-search-toggle')
            .addEventListener('click', () => {
                setTimeout(() => {
                    primarySearchInput.focus()
                }, 50)
            })
    }

    AOS.init()

    toggleClass.init()
    Swipers.init()
    formAnimation.init()
    Accordion.init()
    Menu.init()
    toggle.init()

    if ($('#map').length !== 0) {
        if (window.matchMedia('(min-width: 768px)').matches) {
            Map.init(desktopMapOptionsCommunity)
        } else {
            Map.init(mobileMapOptionsCommunity)
        }
    }

    if ($('#map-detail').length !== 0) {
        Map.init(mapOptionsDetail)
    }

    pedInit()
    toggleForm()
    calcOffset()
    togglePasswordVisibility()
    initAlert('alert-1')
    initOdometer()
}

function calcOffset() {
    resize()

    window.addEventListener('resize', resize)

    function resize() {
        const el = document.querySelector('.js-offset')
        const contentBottom = document.querySelectorAll('.js-text-offset-bottom')
        const contentTop = document.querySelectorAll('.js-text-offset-top')

        if (el) {
            const height = el.offsetHeight

            if (contentBottom.length > 0) {
                each(contentBottom, item => {
                    item.style.paddingBottom = `${height / 2}px`
                })
            }

            if (contentTop.length > 0) {
                each(contentTop, item => {
                    item.style.paddingTop = `${height / 2}px`
                })
            }
        }
    }
}

function toggleForm() {
    const button = $('.js-form-click-button')
    const container = $('.js-remove-border')
    const form = $('.js-toggle-form')


    if (button.length > 0 && container.length > 0 && form.length > 0) {

        button.click(() => {
            button.hide(250)
            container.removeClass('section__form-block--no-border')
            form.slideDown(250)
        })
    }
}

function togglePasswordVisibility() {
    const show = $('.js-pass-show')
    const hide = $('.js-pass-hide')

    if (show.length > 0 && hide.length > 0) {
        show.click(function () {
            toggleInput($(this), 'hide')
        })

        hide.click(function () {
            toggleInput($(this), 'show')
        })
    }

    function toggleInput(elem, state) {
        const type = (state === 'show' && typeof state === 'string') ? 'password' : 'text'

        elem.hide(0)
        elem.siblings(`.js-pass-${state}`).show(0)
        elem.parents('.form__label').find('input').attr('type', type)
    }
}

function initAlert(id) {

    const storage = localStorage.getItem(`leaf:alert:${id}`)
    const alertClose = $(`#${id} .js-alert-close`)

    if (storage) {
        if (storage !== 'opened') {
            alertClose.parent().hide(0)
        } else {
            alertClose.parent().show(250)
        }
    } else {
        localStorage.setItem(`leaf:alert:${id}`, 'opened')
    }

    alertClose.click(function () {
        $(this).parent().hide(250)
        localStorage.setItem(`leaf:alert:${id}`, 'closed')
    })
}

function initOdometer() {
    window.odometerOptions = {
        auto: false,
    }

    document.querySelectorAll('.odometer')
        .forEach(item => {
            const content = +item.dataset.value

            const od = new Odometer({
                el: item,
                format: '',
                theme: 'default',
            })

            item.innerText = 0
            setTimeout(() => {
                item.parentNode.classList.remove('hidden')
                od.update(content)
            }, 150)
        })
}