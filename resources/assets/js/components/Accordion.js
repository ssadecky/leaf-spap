import { each } from '../helpers/helpers'

export default {
    init() {
        this.initListeners()
    },
    initListeners() {
        const selector = document.querySelectorAll('.js-accordion')

        if (selector.length > 0) {
            each(selector, item => {
                const items = item.querySelectorAll('.js-accordion-toggle')

                each(items, el => {
                    el.addEventListener('click', (ev) => {
                        ev.preventDefault()
                        ev.stopPropagation()

                        if (el.classList.contains('is-active')) {
                            this.hide(el)
                        } else {
                            this.show(items, el)
                        }
                    })
                })
            })
        }
    },
    show(collection, el) {
        const content = el.parentNode.querySelector('.js-content')

        each(collection, item => {
            const contentLast = item.parentNode.querySelector('.is-expanded')

            item.classList.remove('is-active')

            if (contentLast) {
                contentLast.classList.remove('is-expanded')

                requestAnimationFrame(() => {
                    contentLast.style.height = `${content.offsetHeight}px`

                    requestAnimationFrame(() => {
                        contentLast.style.height = '0'
                    })
                })
            }
        })

        el.classList.add('is-active')
        content.classList.add('is-expanded')
        content.style.height = `${content.scrollHeight}px`
    },
    hide(el) {
        const content = el.parentNode.querySelector('.js-content')

        el.classList.remove('is-active')
        content.classList.remove('is-expanded')

        requestAnimationFrame(() => {
            content.style.height = `${content.offsetHeight}px`

            requestAnimationFrame(() => {
                content.style.height = '0'
            })
        })
    },
}
