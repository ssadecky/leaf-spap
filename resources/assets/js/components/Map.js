import MarkerClusterer from '@google/markerclusterer'

export default {
    init(customOptions) {

        const defaultOptions = {
            zoom: 1,
            disableDefaultUI: true,
            draggable: true, // disable on mobile ?
            zoomControl: true,
            scrollwheel: false,
            center: { lat: -28.024, lng: 140.887 },
            styles: [
                {
                    'featureType': 'all',
                    'elementType': 'labels.text',
                    'stylers': [
                        {
                            'weight': '1',
                        },
                        {
                            'gamma': '0.96',
                        },
                        {
                            'lightness': '5',
                        },
                    ],
                },
                {
                    'featureType': 'landscape.natural',
                    'elementType': 'geometry.fill',
                    'stylers': [
                        {
                            'visibility': 'off',
                        },
                        {
                            'hue': '#ff0000',
                        },
                    ],
                },
                {
                    'featureType': 'landscape.natural',
                    'elementType': 'labels.text',
                    'stylers': [
                        {
                            'visibility': 'on',
                        },
                        {
                            'color': '#478b54',
                        },
                    ],
                },
                {
                    'featureType': 'poi',
                    'elementType': 'geometry.fill',
                    'stylers': [
                        {
                            'visibility': 'on',
                        },
                        {
                            'color': '#40b153',
                        },
                    ],
                },
                {
                    'featureType': 'road',
                    'elementType': 'geometry',
                    'stylers': [
                        {
                            'lightness': '100',
                        },
                        {
                            'visibility': 'simplified',
                        },
                        {
                            'hue': '#00ff33',
                        },
                    ],
                },
                {
                    'featureType': 'road',
                    'elementType': 'labels',
                    'stylers': [
                        {
                            'visibility': 'off',
                        },
                    ],
                },
                {
                    'featureType': 'transit.line',
                    'elementType': 'geometry',
                    'stylers': [
                        {
                            'visibility': 'on',
                        },
                        {
                            'lightness': 700,
                        },
                        {
                            'color': '#f7f8fa',
                        },
                    ],
                },
                {
                    'featureType': 'water',
                    'elementType': 'all',
                    'stylers': [
                        {
                            'color': '#098B91',
                        },
                    ],
                },
            ],
        }

        const options = { ...defaultOptions, ...customOptions }

        const { locations } = options

        /**
         * Load YouTube iFrame API
         */
        function loadGoogleMapsApi() {
            const youTubeIframeApi = `https://maps.googleapis.com/maps/api/js?key=AIzaSyAxjwMduDLRwsiyZLw7VFe-2raNQ_15Ieg&callback=initMap&v=3.34`
            const tag = document.createElement('script')

            tag.src = youTubeIframeApi

            const firstScriptTag = document.getElementsByTagName('script')[0]

            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag)
        }

        loadGoogleMapsApi()

        function initMap() {
            const map = new google.maps.Map(document.querySelector('.js-map'), options)
            const labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

            const markers = locations.map((location, i) => {
                return new google.maps.Marker({
                    position: location,
                    label: labels[i % labels.length],
                })
            })

            const markerCluster = new MarkerClusterer(map, markers,
                { imagePath: '/images/markers/m' })
        }

        window.initMap = initMap
    },

}