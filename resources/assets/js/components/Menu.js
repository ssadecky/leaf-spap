import { each, parent } from '../helpers/helpers'

export default {
    options: {
        body: document.body,
        wrapper: '.js-slide-menu__wrapper',
        menu: '.js-menu',
        scope: '.js-menu-scope',
        type: 'slide-top',
        selector: '',
    },

    init() {
        const selector = document.querySelectorAll('.js-open-menu')

        each(selector, (el) => {
            let scopeEl = null
            let menu = null

            this.options = Object.assign(this.options, el.dataset)

            if (this.options.scope) {
                scopeEl = parent(el, this.options.scope)

                menu = scopeEl.querySelector('.js-menu')
                menu.classList.add(`slide-menu--${this.options.type}`)

            } else {
                menu = this.options.body.querySelector('.js-menu')
                menu.classList.add(`slide-menu--${this.options.type}`)
            }

            this.initListeners(el, menu, scopeEl)
        })
    },
    initListeners(el, menu, scope) {
        const submenuTogglers = scope.querySelectorAll('.js-has-sub-menu')

        if (submenuTogglers.length > 0) {
            each(submenuTogglers, (item) => {
                item.addEventListener('click', (e) => {
                    e.preventDefault()

                    item.classList.toggle('is-active')
                    item.parentNode.querySelector('.js-sub-menu')
                        .classList.toggle('is-expanded')
                })
            })
        }

        el.addEventListener('click', (ev) => {

            ev.preventDefault()

            if (el.classList.contains('is-active')) {
                this.close(el, menu, scope)
            } else {
                this.open(el, menu, scope)
            }
        })

        document.addEventListener('keydown', (e) => {
            if (e.key === 'Escape') {
                this.close(el, menu, scope)
            }
        })
    },
    open(el, menu, scope) {
        scope.classList.add('has-active-slide-menu')
        scope.querySelector(`${this.options.wrapper}`).classList.add(`has-${this.type}`)
        menu.classList.add('is-active')
        el.classList.add('is-active')
    },
    close(el, menu, scope) {
        scope.classList.remove('has-active-slide-menu')
        scope.querySelector(`${this.options.wrapper}`).classList.remove(`has-${this.type}`)
        menu.classList.remove('is-active')
        el.classList.remove('is-active')
    },
}
