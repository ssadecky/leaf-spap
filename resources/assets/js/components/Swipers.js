import $ from 'jquery'
import ProgressBar from 'progressbar.js'
import Swiper from 'swiper/dist/js/swiper.js'
import { breakpoints } from '../helpers/helpers'

const Swipers = {
    init() {
        this.initStories()
        this.initStories2()
        this.initPartners()
        this.initArticlesMobile()
    },
    initStories() {
        /**
         * Swiper stories
         */
        const $storiesCirclesSwiper = $('.js-swiper-stories-circles')
        const $storiesCirclesSwiperSlides = $('.js-swiper-stories-circles').find('.swiper-slide')
        const $storiesSwiper = $('.js-swiper-stories')
        const storiesCount = $storiesSwiper.find('.swiper-slide:not(.swiper-slide-duplicate)').length

        if ($storiesSwiper.length > 0) {
            const defaultCirclesOptions = {
                allowTouchMove: false,
                centeredSlides: true,
                slidesPerView: 'auto',
                slideToClickedSlide: true,
                on: {
                    slideChangeTransitionEnd: transitionEndCircles,
                },
            }

            const looping = ($storiesCirclesSwiperSlides.length > 10)
            if (looping) {
                defaultCirclesOptions.loop = true
                defaultCirclesOptions.loopedSlides = 50
                defaultCirclesOptions.loopAdditionalSlides = 50
            }

            const speed = 300
            const autoplayTime = 15000
            const defaultOptions = {
                slidesPerView: 1,
                speed,
                loop: true,
                loopedSlides: 50,
                loopAdditionalSlides: 50,
                autoplay: {
                    delay: autoplayTime,
                    disableOnInteraction: false,
                },
                navigation: {
                    nextEl: '.js-swiper-stories-next',
                    prevEl: '.js-swiper-stories-prev',
                },
                on: {
                    init: transitionEnd,
                    slideChangeTransitionEnd: transitionEnd,
                },
            }

            let initialized = false
            let clicked = false

            const $swiper = new Swiper($storiesSwiper, defaultOptions)
            const $circlesSwiper = new Swiper($storiesCirclesSwiper, defaultCirclesOptions)

            /**
             * On init does not work so we call first time manually.
             */
            transitionEndCircles()

            function transitionEndCircles() {
                /**
                 * Progress bar
                 */
                const $active = $('.js-swiper-stories-circles .swiper-slide-active .js-swiper-timer')
                let $progress = $('#js-swiper-progress')
                $progress.remove()
                $progress = $('<div id=\'js-swiper-progress\' class=\'swiper__progress\'></div>')

                $progress.appendTo($active)
                const bar = new ProgressBar.Circle('#js-swiper-progress', {
                    strokeWidth: 3,
                    easing: 'linear',
                    duration: autoplayTime - speed,
                    color: '#40b153',
                    trailColor: '#fff',
                    trailWidth: 2,
                    svgStyle: null,
                })
                bar.animate(1.0)

                if (!this) {
                    return
                }

                /**
                 * Prevent slide from another swiper
                 */
                if (clicked) {
                    clicked = false
                    return
                }

                let clickedIndex = this.realIndex
                if ($swiper && initialized) {
                    clickedIndex %= storiesCount// + 1
                    clicked = true
                    $swiper.slideTo(clickedIndex)
                }
                initialized = true
            }

            function transitionEnd() {
                /**
                 * Prevent slide from another swiper
                 */
                if (clicked) {
                    clicked = false
                    return
                }

                const index = this.realIndex
                if ($circlesSwiper) {
                    clicked = true
                    const to = looping ? index + storiesCount : index
                    $circlesSwiper.slideTo(to)
                }
            }
        }
    },
    initStories2() {
        const $storiesSwiper = $('.js-swiper-stories-type2')

        if ($storiesSwiper.length > 0) {
            const speed = 300
            const autoplayTime = 15000
            const defaultOptions = {
                slidesPerView: 1,
                speed,
                loop: true,
                loopedSlides: 50,
                loopAdditionalSlides: 50,
                autoplay: {
                    delay: autoplayTime,
                    disableOnInteraction: false,
                },
                navigation: {
                    nextEl: '.js-swiper-stories-next',
                    prevEl: '.js-swiper-stories-prev',
                },
                pagination: {
                    el: '.swiper-pagination',
                    type: 'bullets',
                    clickable: true,
                },
            }

            const $swiper = new Swiper($storiesSwiper, defaultOptions)
        }

    },
    initPartners() {
        const $partnersSwiper = $('.js-swiper-partners')

        if ($partnersSwiper.length > 0) {
            const numberOfSlides = {
                desktop: 7,
                tablet: 4,
            }
            const speed = 300
            const autoplayTime = 15000
            const defaultOptions = {
                slidesPerView: numberOfSlides.desktop,
                slidesPerGroup: numberOfSlides.desktop,
                spaceBetween: 30,
                loop: true,
                breakpoints: {
                    728: {
                        slidesPerView: numberOfSlides.tablet,
                        slidesPerGroup: numberOfSlides.tablet,
                    },
                },
                speed,
                autoplay: {
                    delay: autoplayTime,
                    disableOnInteraction: false,
                },
                pagination: {
                    el: '.swiper-pagination',
                    type: 'bullets',
                    clickable: true,
                },
            }

            const $swiper = new Swiper($partnersSwiper, defaultOptions)
        }
    },
    initArticlesMobile() {
        const $articlesSwiper = $('.js-swiper-articles')

        if ($articlesSwiper.length > 0) {
            const speed = 300
            const autoplayTime = 15000
            const defaultOptions = {
                slidesPerView: 1,
                loop: true,
                speed,
                autoplay: {
                    delay: autoplayTime,
                    disableOnInteraction: false,
                },
                pagination: {
                    el: '.swiper-pagination',
                    type: 'bullets',
                    clickable: true,
                },
                init: false,
            }

            const closuredSwiper = this.articlesMobileOnResize()

            closuredSwiper($articlesSwiper, defaultOptions)

            breakpoints.break_t_max.addListener(() => {
                closuredSwiper($articlesSwiper, defaultOptions)
            })
        }
    },
    articlesMobileOnResize() {
        let $swiper

        return (ele, options) => {
            if (breakpoints.break_t_max.matches) {
                if (!$swiper || $swiper.destroyed) {
                    $swiper = new Swiper(ele, options)
                    $swiper.init()
                }
            } else if (typeof $swiper !== 'undefined') {
                $swiper.destroy()
            }
        }
    },
}

export default Swipers
