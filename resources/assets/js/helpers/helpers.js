/**
 * Each helper function.
 */
export const each = (collection, callback) => {
    // eslint-disable-next-line
    for (let i = 0; i < collection.length; i++) {
        const item = collection[i]
        callback(item)
    }
}

/**
 * Parent helper function.
 * Searches up the DOM tree for an element with set class
 * @param el {HTMLElement}
 * @param myClass {string}
 * @returns {HTMLElement}
 */
export const parent = (el, myClass) => {
    if (el.parentNode.classList.contains(myClass)) {
        return el.parentNode
    }

    return parent(el.parentNode, myClass)
}

export const breakpoints = {
    break_t_max: window.matchMedia('(max-width: 729px)'),
}