const escClose = (ev, closeFunction) => {
    if (ev.keyCode === 27) {
        ev.preventDefault()
        closeFunction()
    }
}

let scrollTop

export default {
    open(closeFunction) {
        scrollTop = window.pageYOffset || document.body.scrollTop
        document.body.style.width = `${document.body.clientWidth}px`
        document.body.classList.add('open-popup')
        document.body.style.top = `-${scrollTop}px`
        if (closeFunction) {
            window.addEventListener('keydown', (ev) => {
                escClose(ev, closeFunction)
            })
        }
    },
    close() {
        window.removeEventListener('keydown', escClose)
        document.body.style.top = null
        document.body.style.width = null
        document.body.classList.remove('open-popup')
        window.scrollTo(0, scrollTop)
    },
}
