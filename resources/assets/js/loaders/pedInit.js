import Vue from 'vue'

import selectBox from '../components/SelectBox.vue'

export default (element = document) => {
    element.querySelectorAll('.ped').forEach(item => {
        if (!item) return

        // eslint-disable-next-line no-new
        new Vue({
            el: item,

            components: {
                selectBox,
            },

            data: {
                urlParams: [],
            },

            created() {
                this.showQueryParams()
            },

            methods: {
                showQueryParams() {
                    const urlParams = new URLSearchParams(window.location.search)

                    for (const pair of urlParams.entries()) {
                        const tempObject = { [pair[0]]: pair[1] }

                        this.urlParams.push(tempObject)
                    }
                },
            },
        })
    })
}
