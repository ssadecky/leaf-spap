import { each } from '../helpers/helpers'

export default {
    init() {
        const el = document.querySelectorAll('.js-animate-form')

        if (el.length > 0) {
            each(el, (item) => {
                this.initListeners(item)
            })
        }

        setTimeout(() => {
            each(el, (item) => {
                this.initInputsWithValue(item)
            })
        })
    },

    /**
     * Animate label to top pos
     * @param item {HTMLElement}
     */
    onFocus(item) {
        item.parentNode
            .querySelector('.form__label-text')
            .classList
            .add('form__label-text--focus')
    },

    /**
     * Animate label to placeholder pos
     * if the input doesnt have any value
     * @param item {HTMLElement}
     */
    onBlur(item) {
        if (item.value === '') {
            item.previousElementSibling.classList.remove('form__label-text--focus')
        }
    },

    /**
     * Base init method to init all the listeners for
     * label animation and and radio highlighter
     */
    initListeners(ele) {
        const inputLabels = ele.querySelectorAll('.js--input')

        each(inputLabels, item => {
            item.addEventListener('focus', () => {
                this.onFocus(item)
            })
            item.addEventListener('blur', () => {
                this.onBlur(item)
            })
        })
    },

    /**
     * Iterate over text inputs with value
     * and trigger onFocus() method
     * Thus seamlessly switching the label to top position
     */
    initInputsWithValue(el) {
        const inputLabels = el.querySelectorAll('.js--input')

        each(inputLabels, item => {
            if (item.value !== '') {
                this.onFocus(item)
            }
        })

        /**
         * Delay add transition class
         */
        setTimeout(() => {
            each(inputLabels, item => {
                item.parentNode
                    .querySelector('.form__label-text')
                    .classList
                    .add('form__label-transitions')
            })
        })
    },

    // /**
    //  * Duplicate declaration of several needed binded methods.
    //  * Mostly because directive lacks global methods object
    //  * @param el
    //  */
    // componentUpdated: el => {
    //     const onFocus = item => {
    //         item.parentNode
    //             .querySelector('.form__label-text')
    //             .classList.add('form__label-text--focus')
    //     }
    //
    //     const initInputsWithValue = () => {
    //         const inputLabels = el.querySelectorAll('.js--input')
    //
    //         inputLabels.forEach(item => {
    //             if (item.value !== '') {
    //                 onFocus(item)
    //             }
    //         })
    //     }
    //
    //     initInputsWithValue()
    // },
}
