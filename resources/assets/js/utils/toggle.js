export default {
    init() {
        document
            .querySelectorAll('[data-toggle]')
            .forEach(el => {
                let scope = el.dataset.scope ? el.closest(el.dataset.scope) : document
                if (el.dataset.scope === 'this') {
                    scope = el
                }

                const toggle = scope.querySelector(el.dataset.toggletoggle || '.js-toggle-element')

                function show() {
                    toggle.style.height = `${toggle.scrollHeight}px`
                }

                function hide() {
                    requestAnimationFrame(() => {
                        toggle.style.height = `${toggle.offsetHeight}px`
                        requestAnimationFrame(() => {
                            toggle.style.height = '0'
                        })
                    })
                }

                switch (el.dataset.toggleMode) {
                case 'hover':
                    el.addEventListener('mouseenter', show)
                    el.addEventListener('mouseleave', hide)
                    break
                default:
                case 'click': {
                    const isOffClick = el.dataset.toggleOffclick !== undefined
                    if (isOffClick) {
                        toggle.addEventListener('click', (ev) => {
                            ev.stopPropagation()
                        })
                    }
                    el.addEventListener('click', (ev) => {
                        ev.preventDefault()
                        ev.stopPropagation()
                        if (toggle.clientHeight) {
                            hide()
                            if (isOffClick) {
                                document.removeEventListener('click', hide)
                            }
                        } else {
                            show()
                            if (isOffClick) {
                                document.addEventListener('click', hide)
                            }
                        }
                    })
                    break
                }
                }
            })
    },
}
