import { each } from '../helpers/helpers'

/**
 * ToggleClass
 * Toggle class on selected elements
 * use '.js-toggle-class-trigger' class on trigger element
 * Options: (use data-options= {})
 * - selector {String} = REQUIRED, selector for element to toggle
 * - group {String} = OPTIONAL, only one element in group will have defined class | ''
 * - offclick {Boolean} = OPTIONAL, whether class should be removed on click outside element | false
 * - class {String} = OPTIONAL, class name to toggle on elements | 'is-active'
 * - toggle {Boolean} = OPTIONAL, toggle element on click | false
 */

export default {
    init() {
        each(document.querySelectorAll('.js-toggle-class-trigger'), (item) => {
            const options = item.dataset
            const toggle = document.querySelector(options.selector)
            const group = document.querySelectorAll(options.group) || []
            const isOffClick = options.offclick === 'true'
            const cls = options.class || 'is-active'
            const isToggle = options.toggle === 'true'

            if (!toggle) {
                // eslint-disable-next-line
                console.error(`ToggleClass - Missing toggle element: ${options.selector}`)
                return
            }

            const show = () => {

                each(group, e => {
                    e.classList.remove(cls)
                })
                item.classList.add(cls)
                toggle.classList.add(cls)
            }

            const hide = () => {
                item.classList.remove(cls)
                toggle.classList.remove(cls)
            }

            if (isOffClick) {
                toggle.addEventListener('click', (ev) => {
                    ev.stopPropagation()
                })
            }

            item.addEventListener('click', (ev) => {
                ev.preventDefault()
                ev.stopPropagation()
                if (isToggle) {
                    hide()
                    if (isOffClick) {
                        document.removeEventListener('click', hide)
                    }
                } else {
                    show()
                    if (isOffClick) {
                        document.addEventListener('click', hide)
                    }
                }
            })
        })
    },
}
